using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroAnimada : MonoBehaviour
{

    public GameObject player;
    public GameObject playerAnim;
    public GameObject cameraCinem;

    void Update()
    {
        StartCoroutine(tiempo());
    }

    IEnumerator tiempo()
    {
        yield return new WaitForSeconds(57f);
        Destroy(cameraCinem, 0.05f);
        Destroy(playerAnim, 0.05f);
        player.SetActive(true);

    }
}
