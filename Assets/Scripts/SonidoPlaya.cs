using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonidoPlaya : MonoBehaviour
{
    public Transform jugador; // Referencia al transform del jugador
    public float distanciaMaxima = 60.0f; // Distancia m�xima a la que se escuchar� el sonido
    private AudioSource audioSource; // Referencia al componente AudioSource del objeto

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        // Calcula la distancia entre el jugador y el objeto
        float distancia = Vector3.Distance(jugador.position, transform.position);

        // Calcula el volumen basado en la distancia relativa a la distancia m�xima
        float volumen = 3.0f - (distancia / distanciaMaxima);

        // Limita el volumen entre 0 y 1
        volumen = Mathf.Clamp01(volumen);

        // Asigna el volumen al AudioSource
        audioSource.volume = volumen;
    }
}