using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Playables;

[RequireComponent(typeof(Collider))]

public class TimelineActivation : MonoBehaviour
{
    public PlayableDirector playableDirector;
    public string playerTAG;
    public Transform interactionLocation;
    public bool autoActivate = false;

    public bool interact { get; set; }

    [Header("Activation Zone Events")]
    public UnityEvent onPlayerEnter;
    public UnityEvent onPlayerExit;

    [Header("TimeLine Events")]
    public UnityEvent onTimeLineStart;
    public UnityEvent onTimeLineEnd;

    private bool isPlaying;
    private bool playerInside;
    private Transform playerTransform;


    private void Update()
    {
        if(playerInside && !isPlaying)
        {
            if (interact||autoActivate)
            {
                PlayTimeLine();
            }
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals(playerTAG))
        {
            playerInside = true;
            playerTransform = other.transform;
            onPlayerEnter.Invoke();
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag.Equals(playerTAG))
        {
            playerInside = false;
            playerTransform = null;
            onPlayerExit.Invoke();
        }
    }

    private void PlayTimeLine()
    {
        if(playerTransform && interactionLocation)
        {
            playerTransform.SetPositionAndRotation(interactionLocation.position, interactionLocation.rotation);
        }

        if(autoActivate)
        {
            playerInside = false;
        }

        if(playableDirector)
        {
            playableDirector.Play();
        }
    }

    private IEnumerator waitForTimeLineToEnd()
    {
        onTimeLineStart.Invoke();

        float timelineDuration = (float)playableDirector.duration;

        while (timelineDuration > 0)
        {
            timelineDuration = Time.deltaTime;
            yield return null;
        }

        isPlaying = false;

        onTimeLineEnd.Invoke();
    }


}